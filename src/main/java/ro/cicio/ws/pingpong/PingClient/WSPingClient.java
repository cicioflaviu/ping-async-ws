package ro.cicio.ws.pingpong.PingClient;

import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketUpgradeHandler;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WSPingClient {

    private static final String PONG_SERVER_URL = "ws://localhost:8080/ws_ping";
    private static WebSocket webSocket;
    private static WSPongClientListener pongClientListener;

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        initializeClient();
        asyncPongClient();
    }

    private static void asyncPongClient() throws InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        while (true) {
            executorService.submit(new Runnable() {
                public void run() {

                    int pingId = new Random().nextInt(10000);
                    long startTime = System.nanoTime();
                    webSocket.sendTextFrame("PING " + pingId);
                    System.out.println("Thread " + Thread.currentThread().getId() + ": sent Ping " + pingId);
                    pongClientListener.addPing(pingId);
                    boolean responseReceived = false;
                    while (!responseReceived) {
                        responseReceived = pongClientListener.receivedPong(pingId);
                    }
                    long endTime = System.nanoTime();
                    long totalTime = endTime - startTime;
                    System.out.println("Thread " + Thread.currentThread().getId() + ": PING with ID " + pingId + " completed in " + (int) (totalTime / 1000000) + " milliseconds");
                }
            });
            Thread.sleep(30000);
        }
    }

    private static void initializeClient() throws InterruptedException, ExecutionException {
        DefaultAsyncHttpClient client = new DefaultAsyncHttpClient();
        WebSocketUpgradeHandler.Builder upgradeBuilder = new WebSocketUpgradeHandler.Builder();
        WebSocketUpgradeHandler wsUpgradeHandler = upgradeBuilder.build();
        webSocket = client.prepareGet(PONG_SERVER_URL)
                .execute(wsUpgradeHandler)
                .get();
        pongClientListener = new WSPongClientListener();
        webSocket.addWebSocketListener(pongClientListener);
    }
}

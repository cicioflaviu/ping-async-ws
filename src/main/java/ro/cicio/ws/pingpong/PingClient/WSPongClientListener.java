package ro.cicio.ws.pingpong.PingClient;

import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketListener;

import java.util.ArrayList;
import java.util.List;

public class WSPongClientListener implements WebSocketListener {
    private static List<Integer> pings;

    public WSPongClientListener() {
        pings = new ArrayList<Integer>();
    }

    public void onOpen(WebSocket websocket) {
        System.out.println("Connection established with " + websocket.getLocalAddress());
    }

    public void onClose(WebSocket websocket, int code, String reason) {
        System.out.println("Connection closed with code " + code + " and the following reason: " + reason);
    }

    public void onError(Throwable t) {
        System.out.println("Something happened: " + t);
    }

    public void onTextFrame(String payload, boolean finalFragment, int rsv) {
        pings.remove(Integer.valueOf(payload.split(" ")[1]));
    }

    public List<Integer> addPing(int pingId) {
        pings.add(pingId);
        return pings;
    }

    public boolean receivedPong(int pingId) {
        return !pings.contains(pingId);
    }
}

package ro.cicio.ws.pingpong.PongServer;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;

public class WSPingFrameHandler extends SimpleChannelInboundHandler<WebSocketFrame> {

    protected void channelRead0(ChannelHandlerContext channelHandlerContext, WebSocketFrame webSocketFrame) throws Exception {
        if (isPing(webSocketFrame)) {
            String pingId = ((TextWebSocketFrame) webSocketFrame).text().split(" ")[1];
            String response = "PONG " + pingId;
            System.out.println("Thread " + Thread.currentThread().getId() + ": Received PING with id: " + pingId);
            Thread.sleep(Long.parseLong(pingId));
            channelHandlerContext.channel().writeAndFlush(new TextWebSocketFrame(response));
            System.out.println("Thread " + Thread.currentThread().getId() + ": Sent response for PING with id: " + pingId);
        } else
            throw new UnsupportedOperationException("Unsupported frame: " + webSocketFrame.getClass().getName());
    }

    private boolean isPing(WebSocketFrame webSocketFrame) {
        return webSocketFrame instanceof TextWebSocketFrame && ((TextWebSocketFrame) webSocketFrame).text().toUpperCase().contains("PING");
    }
}
